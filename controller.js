//SAKTI PARDANO
const response = require("./res");
const connection = require("./conn");

exports.getProduct = function (req, res) {
    connection.query(
        "select sku,product_name,stocks from products order by sku desc",
        function (error, rows) {
            if (error) {
                console.log(error);
            } else {
                response.ok(rows, res);
            }
        }
    );
};

exports.index = function (req, res) {
    response.ok("berhasil", res);
};
