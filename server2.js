//SAKTI PARDANO
const express = require("express");
const port = process.env.PORT || 9000;
const bodyParser = require("body-parser");
const controller = require("./controller");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const routes = require("./routes");
routes(app);

app.listen(port);
